﻿<!DOCTYPE html>
<html>
  <head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta name="keywords" content="Transfers - Private Transport and Car Hire HTML Template">
	<meta name="description" content="Transfers - Private Transport and Car Hire HTML Template">
	<meta name="author" content="themeenergy.com">
	
	<title>Transfers - Search results</title>
	
	<link rel="stylesheet" href="css\styler.css">
	<link rel="stylesheet" href="css\theme-pink.css" id="template-color">
	<link rel="stylesheet" href="css\style.css">
	<link rel="stylesheet" href="css\animate.css">
	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Raleway:400,500,600,700|Montserrat:400,700">
	<link rel="shortcut icon" href="images\favicon.ico">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  
  <body>
	<!-- Preloader -->
	<div class="preloader">
		<div id="followingBallsG">
			<div id="followingBallsG_1" class="followingBallsG"></div>
			<div id="followingBallsG_2" class="followingBallsG"></div>
			<div id="followingBallsG_3" class="followingBallsG"></div>
			<div id="followingBallsG_4" class="followingBallsG"></div>
		</div>
	</div>
	<!-- //Preloader -->
	
    <!-- Header -->
	<header class="header" role="banner">
		<div class="wrap">
			<!-- Logo -->
			<div class="logo">
				<a href="index-1.htm" title="Transfers"><img src="images\transfers.jpg" alt="Transfers"></a>
			</div>
			<!-- //Logo -->
			
			<!-- Main Nav -->
			<nav role="navigation" class="main-nav">
				<ul>
					<li><a href="index-1.htm" title="">Home</a></li>
					<li><a href="destinations.htm" title="Destinations">Destinations</a>
						<ul>
							<li><a href="destination-single.htm" title="Single destination">Single destination</a></li>
							<li><a href="destination-micro.htm" title="Micro destination">Micro destination</a></li>
						</ul>
					</li>
					<li><a href="tailor-made.htm" title="Tailor made">Tailor made</a></li>
					<li><a href="blog.htm" title="Blog">Blog</a>
						<ul>
							<li><a href="blog.htm" title="Post">Blog list</a></li>
							<li><a href="blog2.htm" title="Post">Blog grid</a></li>
							<li><a href="blog-single.htm" title="Post">Post</a></li>
						</ul>
					</li>
					<li><a href="contact.htm" title="Contact">Contact</a></li>
					<li><a href="#" title="Pages">Pages</a>
						<div>
							<div class="one-fourth">
								<h2>Common</h2>
								<ul>
									<li><a href="left-sidebar-page.htm" title="Left sidebar page">Left sidebar page</a></li>
									<li><a href="right-sidebar-page.htm" title="Right sidebar page">Right sidebar page</a></li>
									<li><a href="both-sidebar-page.htm" title="Both sidebars page">Both sidebars page</a></li>
									<li><a href="full-width-page.htm" title="Full width page">Full width page</a></li>
								</ul>
							</div>
							<div class="one-fourth">
								<h2>Booking</h2>
								<ul>
									<li><a href="search-results.htm" title="Search results page">Search results page</a></li>
									<li><a href="booking-step1.htm" title="Booking step 1">Booking step 1</a></li>
									<li><a href="booking-step2.htm" title="Booking step 2">Booking step 2</a></li>
									<li><a href="booking-step3.htm" title="Booking step 3">Booking step 3</a></li>
								</ul>
							</div>
							<div class="one-fourth">
								<h2>Corporate</h2>
								<ul>
									<li><a href="about.htm" title="About u">About us</a></li>
									<li><a href="services.htm" title="Services">Services</a></li>
									<li><a href="faq.htm" title="Faq">Faq</a></li>
									<li><a href="contact.htm" title="Contact">Contact</a></li>
								</ul>
							</div>
							<div class="one-fourth">
								<h2>Special</h2>
								<ul>
									<li><a href="login.htm" title="Login">Login</a></li>
									<li><a href="register.htm" title="Register">Register</a></li>
									<li><a href="account.htm" title="My account">My account</a></li>
									<li><a href="error.htm" title="404 error">404 error</a></li>
								</ul>
							</div>
						</div>
					</li>
					<li><a href="http://themeforest.net/item/transfers-transport-and-car-hire-html-template/9366018?ref=themeenergy" title="Purchase">Purchase</a></li>
				</ul>
			</nav>
			<!-- //Main Nav -->
		</div>
	</header>
	<!-- //Header -->
	
	<!-- Main -->
	<main class="main" role="main">
		<!-- Search -->
		<div class="advanced-search color">
			<div class="wrap">
				<form role="form">
					<!-- Row -->
					<div class="f-row">
						<div class="form-group datepicker one-third">
							<label for="dep-date">Departure date and time</label>
							<input type="text" id="dep-date" value="28/08/2014">
						</div>
						<div class="form-group select one-third">
							<label>Pick up location</label>
							<select>
								<option>Select location
								<optgroup label="Germany">
									<option value="Berlin Central Train Station">Berlin Central Train Station
									<option value="Berlin Schonefeld Airport" selected="">Berlin Schonefeld Airport
									<option value="Berlin Tegel Airport">Berlin Tegel Airport
									<option value="Bremen Airport">Bremen Airport
									<option value="Cologne Bonn Airport">Cologne Bonn Airport
									<option value="Dortmund Airport">Dortmund Airport
									<option value="Dresden Airport">Dresden Airport
									<option value="Dusseldorf Airport">Dusseldorf Airport
									<option value="Frankfurt Hahn Airport">Frankfurt Hahn Airport
									<option value="Frankfurt International Airport">Frankfurt International Airport
									<option value="Friedrichshafen Airport">Friedrichshafen Airport
									<option value="Hamburg Finkenwerder Airport">Hamburg Finkenwerder Airport
									<option value="Hamburg Port">Hamburg Port
									<option value="Hamburg-Fuhlsbuttel Airport">Hamburg-Fuhlsbuttel Airport
									<option value="Hanover Lengenhagen Airport">Hanover Lengenhagen Airport
									<option value="Kiel Port">Kiel Port
									<option value="Leipzig Halle Airport">Leipzig Halle Airport
									<option value="Munich Airport">Munich Airport
									<option value="Nuremberg Airport">Nuremberg Airport
								</optgroup>
								<optgroup label="Italy">
									<option value="Alghero Airport">Alghero Airport
									<option value="Bari Airport">Bari Airport
									<option value="Bari Port">Bari Port
									<option value="Bergamo Airport">Bergamo Airport
									<option value="Bologna Airport">Bologna Airport
									<option value="Brindisi Airport">Brindisi Airport
									<option value="Cagliari Airport">Cagliari Airport
									<option value="Florence Airport">Florence Airport
									<option value="Florence Train Station">Florence Train Station
									<option value="Genoa Airport">Genoa Airport
									<option value="Genoa Port">Genoa Port
									<option value="Milan Central Train Station">Milan Central Train Station
									<option value="Milan City Centre">Milan City Centre
									<option value="Naples Airport">Naples Airport
								</optgroup>
								<optgroup label="Spain">
									<option value="Alicante Airport">Alicante Airport
									<option value="Almeria Airport">Almeria Airport
									<option value="Barcelona Airport">Barcelona Airport
									<option value="Granada Airport">Granada Airport
									<option value="Jerez de la Frontera">Jerez de la Frontera
									<option value="Leon Airport">Leon Airport
									<option value="Madrid Atocha Train Station">Madrid Atocha Train Station
									<option value="Madrid Barajas Airport">Madrid Barajas Airport
									<option value="Pamplona Airport">Pamplona Airport
									<option value="Santander Airport">Santander Airport
									<option value="Seville Airport">Seville Airport
									<option value="Valencia Airport">Valencia Airport
									<option value="Zaragoza Airport">Zaragoza Airport
								</optgroup>
								<optgroup label="United Kingdom">
									<option value="Belfast City Airport">Belfast City Airport
									<option value="Belfast International Airport">Belfast International Airport
									<option value="Bristol Airport">Bristol Airport
									<option value="Cardiff Airport">Cardiff Airport
									<option value="Edinburgh Airport">Edinburgh Airport
									<option value="Glasgow International Airport">Glasgow International Airport
									<option value="London City Airport">London City Airport
									<option value="London Gatwick Airport">London Gatwick Airport
									<option value="London Heathrow Airport">London Heathrow Airport
									<option value="London Stansted Airport">London Stansted Airport
									<option value="Manchester Airport">Manchester Airport
									<option value="Southampton Airport">Southampton Airport
								</optgroup>
							</select>
						</div>
						<div class="form-group select one-third">
							<label>Drop off location</label>
							<select>
								<option>Select location
								<optgroup label="Germany">
									<option value="Berlin Central Train Station" selected="">Berlin Central Train Station
									<option value="Berlin Schonefeld Airport">Berlin Schonefeld Airport
									<option value="Berlin Tegel Airport">Berlin Tegel Airport
									<option value="Bremen Airport">Bremen Airport
									<option value="Cologne Bonn Airport">Cologne Bonn Airport
									<option value="Dortmund Airport">Dortmund Airport
									<option value="Dresden Airport">Dresden Airport
									<option value="Dusseldorf Airport">Dusseldorf Airport
									<option value="Frankfurt Hahn Airport">Frankfurt Hahn Airport
									<option value="Frankfurt International Airport">Frankfurt International Airport
									<option value="Friedrichshafen Airport">Friedrichshafen Airport
									<option value="Hamburg Finkenwerder Airport">Hamburg Finkenwerder Airport
									<option value="Hamburg Port">Hamburg Port
									<option value="Hamburg-Fuhlsbuttel Airport">Hamburg-Fuhlsbuttel Airport
									<option value="Hanover Lengenhagen Airport">Hanover Lengenhagen Airport
									<option value="Kiel Port">Kiel Port
									<option value="Leipzig Halle Airport">Leipzig Halle Airport
									<option value="Munich Airport">Munich Airport
									<option value="Nuremberg Airport">Nuremberg Airport
								</optgroup>
								<optgroup label="Italy">
									<option value="Alghero Airport">Alghero Airport
									<option value="Bari Airport">Bari Airport
									<option value="Bari Port">Bari Port
									<option value="Bergamo Airport">Bergamo Airport
									<option value="Bologna Airport">Bologna Airport
									<option value="Brindisi Airport">Brindisi Airport
									<option value="Cagliari Airport">Cagliari Airport
									<option value="Florence Airport">Florence Airport
									<option value="Florence Train Station">Florence Train Station
									<option value="Genoa Airport">Genoa Airport
									<option value="Genoa Port">Genoa Port
									<option value="Milan Central Train Station">Milan Central Train Station
									<option value="Milan City Centre">Milan City Centre
									<option value="Naples Airport">Naples Airport
								</optgroup>
								<optgroup label="Spain">
									<option value="Alicante Airport">Alicante Airport
									<option value="Almeria Airport">Almeria Airport
									<option value="Barcelona Airport">Barcelona Airport
									<option value="Granada Airport">Granada Airport
									<option value="Jerez de la Frontera">Jerez de la Frontera
									<option value="Leon Airport">Leon Airport
									<option value="Madrid Atocha Train Station">Madrid Atocha Train Station
									<option value="Madrid Barajas Airport">Madrid Barajas Airport
									<option value="Pamplona Airport">Pamplona Airport
									<option value="Santander Airport">Santander Airport
									<option value="Seville Airport">Seville Airport
									<option value="Valencia Airport">Valencia Airport
									<option value="Zaragoza Airport">Zaragoza Airport
								</optgroup>
								<optgroup label="United Kingdom">
									<option value="Belfast City Airport">Belfast City Airport
									<option value="Belfast International Airport">Belfast International Airport
									<option value="Bristol Airport">Bristol Airport
									<option value="Cardiff Airport">Cardiff Airport
									<option value="Edinburgh Airport">Edinburgh Airport
									<option value="Glasgow International Airport">Glasgow International Airport
									<option value="London City Airport">London City Airport
									<option value="London Gatwick Airport">London Gatwick Airport
									<option value="London Heathrow Airport">London Heathrow Airport
									<option value="London Stansted Airport">London Stansted Airport
									<option value="Manchester Airport">Manchester Airport
									<option value="Southampton Airport">Southampton Airport
								</optgroup>
							</select>
						</div>
					</div>
					<!-- //Row -->
					
					<!-- Row -->
					<div class="f-row">
						<div class="form-group datepicker one-third">
							<label for="ret-date">Return date and time</label>
							<input type="text" id="ret-date" value="02/09/2014">
						</div>
						<div class="form-group select">
							<label>Pick up location</label>
							<select>
								<option>Select location
								<optgroup label="Germany">
									<option value="Berlin Central Train Station" selected="">Berlin Central Train Station
									<option value="Berlin Schonefeld Airport">Berlin Schonefeld Airport
									<option value="Berlin Tegel Airport">Berlin Tegel Airport
									<option value="Bremen Airport">Bremen Airport
									<option value="Cologne Bonn Airport">Cologne Bonn Airport
									<option value="Dortmund Airport">Dortmund Airport
									<option value="Dresden Airport">Dresden Airport
									<option value="Dusseldorf Airport">Dusseldorf Airport
									<option value="Frankfurt Hahn Airport">Frankfurt Hahn Airport
									<option value="Frankfurt International Airport">Frankfurt International Airport
									<option value="Friedrichshafen Airport">Friedrichshafen Airport
									<option value="Hamburg Finkenwerder Airport">Hamburg Finkenwerder Airport
									<option value="Hamburg Port">Hamburg Port
									<option value="Hamburg-Fuhlsbuttel Airport">Hamburg-Fuhlsbuttel Airport
									<option value="Hanover Lengenhagen Airport">Hanover Lengenhagen Airport
									<option value="Kiel Port">Kiel Port
									<option value="Leipzig Halle Airport">Leipzig Halle Airport
									<option value="Munich Airport">Munich Airport
									<option value="Nuremberg Airport">Nuremberg Airport
								</optgroup>
								<optgroup label="Italy">
									<option value="Alghero Airport">Alghero Airport
									<option value="Bari Airport">Bari Airport
									<option value="Bari Port">Bari Port
									<option value="Bergamo Airport">Bergamo Airport
									<option value="Bologna Airport">Bologna Airport
									<option value="Brindisi Airport">Brindisi Airport
									<option value="Cagliari Airport">Cagliari Airport
									<option value="Florence Airport">Florence Airport
									<option value="Florence Train Station">Florence Train Station
									<option value="Genoa Airport">Genoa Airport
									<option value="Genoa Port">Genoa Port
									<option value="Milan Central Train Station">Milan Central Train Station
									<option value="Milan City Centre">Milan City Centre
									<option value="Naples Airport">Naples Airport
								</optgroup>
								<optgroup label="Spain">
									<option value="Alicante Airport">Alicante Airport
									<option value="Almeria Airport">Almeria Airport
									<option value="Barcelona Airport">Barcelona Airport
									<option value="Granada Airport">Granada Airport
									<option value="Jerez de la Frontera">Jerez de la Frontera
									<option value="Leon Airport">Leon Airport
									<option value="Madrid Atocha Train Station">Madrid Atocha Train Station
									<option value="Madrid Barajas Airport">Madrid Barajas Airport
									<option value="Pamplona Airport">Pamplona Airport
									<option value="Santander Airport">Santander Airport
									<option value="Seville Airport">Seville Airport
									<option value="Valencia Airport">Valencia Airport
									<option value="Zaragoza Airport">Zaragoza Airport
								</optgroup>
								<optgroup label="United Kingdom">
									<option value="Belfast City Airport">Belfast City Airport
									<option value="Belfast International Airport">Belfast International Airport
									<option value="Bristol Airport">Bristol Airport
									<option value="Cardiff Airport">Cardiff Airport
									<option value="Edinburgh Airport">Edinburgh Airport
									<option value="Glasgow International Airport">Glasgow International Airport
									<option value="London City Airport">London City Airport
									<option value="London Gatwick Airport">London Gatwick Airport
									<option value="London Heathrow Airport">London Heathrow Airport
									<option value="London Stansted Airport">London Stansted Airport
									<option value="Manchester Airport">Manchester Airport
									<option value="Southampton Airport">Southampton Airport
								</optgroup>
							</select>
						</div>
						<div class="form-group select one-third">
							<label>Drop off location</label>
							<select>
								<option>Select location
								<optgroup label="Germany">
									<option value="Berlin Central Train Station">Berlin Central Train Station
									<option value="Berlin Schonefeld Airport" selected="">Berlin Schonefeld Airport
									<option value="Berlin Tegel Airport">Berlin Tegel Airport
									<option value="Bremen Airport">Bremen Airport
									<option value="Cologne Bonn Airport">Cologne Bonn Airport
									<option value="Dortmund Airport">Dortmund Airport
									<option value="Dresden Airport">Dresden Airport
									<option value="Dusseldorf Airport">Dusseldorf Airport
									<option value="Frankfurt Hahn Airport">Frankfurt Hahn Airport
									<option value="Frankfurt International Airport">Frankfurt International Airport
									<option value="Friedrichshafen Airport">Friedrichshafen Airport
									<option value="Hamburg Finkenwerder Airport">Hamburg Finkenwerder Airport
									<option value="Hamburg Port">Hamburg Port
									<option value="Hamburg-Fuhlsbuttel Airport">Hamburg-Fuhlsbuttel Airport
									<option value="Hanover Lengenhagen Airport">Hanover Lengenhagen Airport
									<option value="Kiel Port">Kiel Port
									<option value="Leipzig Halle Airport">Leipzig Halle Airport
									<option value="Munich Airport">Munich Airport
									<option value="Nuremberg Airport">Nuremberg Airport
								</optgroup>
								<optgroup label="Italy">
									<option value="Alghero Airport">Alghero Airport
									<option value="Bari Airport">Bari Airport
									<option value="Bari Port">Bari Port
									<option value="Bergamo Airport">Bergamo Airport
									<option value="Bologna Airport">Bologna Airport
									<option value="Brindisi Airport">Brindisi Airport
									<option value="Cagliari Airport">Cagliari Airport
									<option value="Florence Airport">Florence Airport
									<option value="Florence Train Station">Florence Train Station
									<option value="Genoa Airport">Genoa Airport
									<option value="Genoa Port">Genoa Port
									<option value="Milan Central Train Station">Milan Central Train Station
									<option value="Milan City Centre">Milan City Centre
									<option value="Naples Airport">Naples Airport
								</optgroup>
								<optgroup label="Spain">
									<option value="Alicante Airport">Alicante Airport
									<option value="Almeria Airport">Almeria Airport
									<option value="Barcelona Airport">Barcelona Airport
									<option value="Granada Airport">Granada Airport
									<option value="Jerez de la Frontera">Jerez de la Frontera
									<option value="Leon Airport">Leon Airport
									<option value="Madrid Atocha Train Station">Madrid Atocha Train Station
									<option value="Madrid Barajas Airport">Madrid Barajas Airport
									<option value="Pamplona Airport">Pamplona Airport
									<option value="Santander Airport">Santander Airport
									<option value="Seville Airport">Seville Airport
									<option value="Valencia Airport">Valencia Airport
									<option value="Zaragoza Airport">Zaragoza Airport
								</optgroup>
								<optgroup label="United Kingdom">
									<option value="Belfast City Airport">Belfast City Airport
									<option value="Belfast International Airport">Belfast International Airport
									<option value="Bristol Airport">Bristol Airport
									<option value="Cardiff Airport">Cardiff Airport
									<option value="Edinburgh Airport">Edinburgh Airport
									<option value="Glasgow International Airport">Glasgow International Airport
									<option value="London City Airport">London City Airport
									<option value="London Gatwick Airport">London Gatwick Airport
									<option value="London Heathrow Airport">London Heathrow Airport
									<option value="London Stansted Airport">London Stansted Airport
									<option value="Manchester Airport">Manchester Airport
									<option value="Southampton Airport">Southampton Airport
								</optgroup>
							</select>
						</div>
					</div>
					<!-- //Row -->
					
					<!-- Row -->
					<div class="f-row">
						<div class="form-group spinner">
							<label for="people">How many people <small>(including children)</small>?</label>
							<input type="number" id="people" min="1" value="2">
						</div>
						<div class="form-group radios">
							<div>
								<input type="radio" name="radio" id="return" value="return">
								<label for="return">Return</label>
							</div>
							<div>
								<input type="radio" name="radio" id="oneway" value="oneway" checked="">
								<label for="oneway">One way</label>
							</div>
						</div>
						<div class="form-group right">
							<button type="submit" class="btn large black">Find a transfer</button>
						</div>
					</div>
					<!--// Row -->
				</form>
			</div>
		</div>
		<!-- //Search -->
		
		<div class="wrap">
			<div class="row">
				<!--- Content -->
				<div class="full-width content">
					<h2>Select transfer type for your DEPARTURE</h2>
					
					<div class="results">
						<!-- Item -->
						<article class="result">
							<div class="one-fourth heightfix"><img src="images\uploads\car.jpg" alt=""></div>
							<div class="one-half heightfix">
								<h3>Private car <a href="javascript:void(0)" class="trigger color" title="Read more">?</a></h3>
								<ul>
									<li>
										<span class="ico people"></span>
										<p>Max <strong>3 people</strong> <br>per vehicle</p>
									</li>
									<li>
										<span class="ico luggage"></span>
										<p>Max <strong>3 suitcases</strong> <br>per vehicle</p>
									</li>
									<li>
										<span class="ico time"></span>
										<p>Estimated time <br><strong>50 mins</strong></p>
									</li>
								</ul>
							</div>
							<div class="one-fourth heightfix">
								<div>
									<div class="price">130,00 <small>USD</small></div>
									<span class="meta">per passenger</span>
									<a href="booking-step1.htm" class="btn grey large">select</a>
								</div>
							</div>
							<div class="full-width information">	
								<a href="javascript:void(0)" class="close color" title="Close">x</a>
								<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
							</div>
						</article>
						<!-- //Item -->
						
						<!-- Item -->
						<article class="result">
							<div class="one-fourth heightfix"><img src="images\uploads\van.jpg" alt=""></div>
							<div class="one-half heightfix">
								<h3>Private shuttle <a href="javascript:void(0)" class="trigger color" title="Read more">?</a></h3>
								<ul>
									<li>
										<span class="ico people"></span>
										<p>Max <strong>3 people</strong> <br>per vehicle</p>
									</li>
									<li>
										<span class="ico luggage"></span>
										<p>Max <strong>3 suitcases</strong> <br>per vehicle</p>
									</li>
									<li>
										<span class="ico time"></span>
										<p>Estimated time <br><strong>50 mins</strong></p>
									</li>
								</ul>
							</div>
							<div class="one-fourth heightfix">
								<div>
									<div class="price">130,00 <small>USD</small></div>
									<span class="meta">per passenger</span>
									<a href="booking-step1.htm" class="btn grey large">select</a>
								</div>
							</div>
							<div class="full-width information">	
								<a href="javascript:void(0)" class="close color" title="Close">x</a>
								<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
							</div>
						</article>
						<!-- //Item -->
						
						<!-- Item -->
						<article class="result">
							<div class="one-fourth heightfix"><img src="images\uploads\van.jpg" alt=""></div>
							<div class="one-half heightfix">
								<h3>Shared shuttle <a href="javascript:void(0)" class="trigger color" title="Read more">?</a></h3>
								<ul>
									<li>
										<span class="ico people"></span>
										<p>Max <strong>3 people</strong> <br>per vehicle</p>
									</li>
									<li>
										<span class="ico luggage"></span>
										<p>Max <strong>3 suitcases</strong> <br>per vehicle</p>
									</li>
									<li>
										<span class="ico time"></span>
										<p>Estimated time <br><strong>50 mins</strong></p>
									</li>
								</ul>
							</div>
							<div class="one-fourth heightfix">
								<div>
									<div class="price">130,00 <small>USD</small></div>
									<span class="meta">per passenger</span>
									<a href="booking-step1.htm" class="btn grey large">select</a>
								</div>
							</div>
							<div class="full-width information">	
								<a href="javascript:void(0)" class="close color" title="Close">x</a>
								<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
							</div>
						</article>
						<!-- //Item -->
						
						<!-- Item -->
						<article class="result">
							<div class="one-fourth heightfix"><img src="images\uploads\bus.jpg" alt=""></div>
							<div class="one-half heightfix">
								<h3>Private bus <a href="javascript:void(0)" class="trigger color" title="Read more">?</a></h3>
								<ul>
									<li>
										<span class="ico people"></span>
										<p>Max <strong>3 people</strong> <br>per vehicle</p>
									</li>
									<li>
										<span class="ico luggage"></span>
										<p>Max <strong>3 suitcases</strong> <br>per vehicle</p>
									</li>
									<li>
										<span class="ico time"></span>
										<p>Estimated time <br><strong>50 mins</strong></p>
									</li>
								</ul>
							</div>
							<div class="one-fourth heightfix">
								<div>
									<div class="price">130,00 <small>USD</small></div>
									<span class="meta">per passenger</span>
									<a href="booking-step1.htm" class="btn grey large">select</a>
								</div>
							</div>
							<div class="full-width information">	
								<a href="javascript:void(0)" class="close color" title="Close">x</a>
								<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
							</div>
						</article>
						<!-- //Item -->
						
						<!-- Item -->
						<article class="result">
							<div class="one-fourth heightfix"><img src="images\uploads\heli.jpg" alt=""></div>
							<div class="one-half heightfix">
								<h3>Private helicopter <a href="javascript:void(0)" class="trigger color" title="Read more">?</a></h3>
								<ul>
									<li>
										<span class="ico people"></span>
										<p>Max <strong>3 people</strong> <br>per vehicle</p>
									</li>
									<li>
										<span class="ico luggage"></span>
										<p>Max <strong>3 suitcases</strong> <br>per vehicle</p>
									</li>
									<li>
										<span class="ico time"></span>
										<p>Estimated time <br><strong>50 mins</strong></p>
									</li>
								</ul>
							</div>
							<div class="one-fourth heightfix">
								<div>
									<div class="price">130,00 <small>USD</small></div>
									<span class="meta">per passenger</span>
									<a href="booking-step1.htm" class="btn grey large">select</a>
								</div>
							</div>
							<div class="full-width information">	
								<a href="javascript:void(0)" class="close color" title="Close">x</a>
								<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
							</div>
						</article>
						<!-- //Item -->
					</div>
					
					<h2>Select transfer for your RETURN</h2>
					
					<div class="results">
						<!-- Item -->
						<article class="result">
							<div class="one-fourth heightfix"><img src="images\uploads\car.jpg" alt=""></div>
							<div class="one-half heightfix">
								<h3>Private car <a href="javascript:void(0)" class="trigger color" title="Read more">?</a></h3>
								<ul>
									<li>
										<span class="ico people"></span>
										<p>Max <strong>3 people</strong> <br>per vehicle</p>
									</li>
									<li>
										<span class="ico luggage"></span>
										<p>Max <strong>3 suitcases</strong> <br>per vehicle</p>
									</li>
									<li>
										<span class="ico time"></span>
										<p>Estimated time <br><strong>50 mins</strong></p>
									</li>
								</ul>
							</div>
							<div class="one-fourth heightfix">
								<div>
									<div class="price">130,00 <small>USD</small></div>
									<span class="meta">per passenger</span>
									<a href="booking-step1.htm" class="btn grey large">select</a>
								</div>
							</div>
							<div class="full-width information">	
								<a href="javascript:void(0)" class="close color" title="Close">x</a>
								<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
							</div>
						</article>
						<!-- //Item -->
						
						<!-- Item -->
						<article class="result">
							<div class="one-fourth heightfix"><img src="images\uploads\van.jpg" alt=""></div>
							<div class="one-half heightfix">
								<h3>Private shuttle <a href="javascript:void(0)" class="trigger color" title="Read more">?</a></h3>
								<ul>
									<li>
										<span class="ico people"></span>
										<p>Max <strong>3 people</strong> <br>per vehicle</p>
									</li>
									<li>
										<span class="ico luggage"></span>
										<p>Max <strong>3 suitcases</strong> <br>per vehicle</p>
									</li>
									<li>
										<span class="ico time"></span>
										<p>Estimated time <br><strong>50 mins</strong></p>
									</li>
								</ul>
							</div>
							<div class="one-fourth heightfix">
								<div>
									<div class="price">130,00 <small>USD</small></div>
									<span class="meta">per passenger</span>
									<a href="booking-step1.htm" class="btn grey large">select</a>
								</div>
							</div>
							<div class="full-width information">	
								<a href="javascript:void(0)" class="close color" title="Close">x</a>
								<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
							</div>
						</article>
						<!-- //Item -->
						
						<!-- Item -->
						<article class="result">
							<div class="one-fourth heightfix"><img src="images\uploads\bus.jpg" alt=""></div>
							<div class="one-half heightfix">
								<h3>Shared bus <a href="javascript:void(0)" class="trigger color" title="Read more">?</a></h3>
								<ul>
									<li>
										<span class="ico people"></span>
										<p>Max <strong>3 people</strong> <br>per vehicle</p>
									</li>
									<li>
										<span class="ico luggage"></span>
										<p>Max <strong>3 suitcases</strong> <br>per vehicle</p>
									</li>
									<li>
										<span class="ico time"></span>
										<p>Estimated time <br><strong>50 mins</strong></p>
									</li>
								</ul>
							</div>
							<div class="one-fourth heightfix">
								<div>
									<div class="price">130,00 <small>USD</small></div>
									<span class="meta">per passenger</span>
									<a href="booking-step1.htm" class="btn grey large">select</a>
								</div>
							</div>
							<div class="full-width information">	
								<a href="javascript:void(0)" class="close color" title="Close">x</a>
								<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
							</div>
						</article>
						<!-- //Item -->
					</div>
				</div>
				<!--- //Content -->
			</div>
		</div>
	</main>
	<!-- //Main -->
	
	<!-- Footer -->
	<footer class="footer black" role="contentinfo">
		<div class="wrap">
			<div class="row">
				<!-- Column -->
				<article class="one-half">
					<h6>About us</h6>
					<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy.</p>
				</article>
				<!-- //Column -->
				
				<!-- Column -->
				<article class="one-fourth">
					<h6>Need help?</h6>
					<p>Contact us via phone or email:</p>
					<p class="contact-data"><span class="ico phone"></span> +1 555 555 555</p>
					<p class="contact-data"><span class="ico email"></span> <a href="mailto:help@transfers.com">help@transfers.com</a></p>
				</article>
				<!-- //Column -->
				
				<!-- Column -->
				<article class="one-fourth">
					<h6>Follow us</h6>
					<ul class="social">
						<li class="facebook"><a href="#" title="facebook">facebook</a></li>
						<li class="twitter"><a href="#" title="twitter">twitter</a></li>
						<li class="gplus"><a href="#" title="gplus">google plus</a></li>
						<li class="linkedin"><a href="#" title="linkedin">linkedin</a></li>
						<li class="vimeo"><a href="#" title="vimeo">vimeo</a></li>
						<li class="pinterest"><a href="#" title="pinterest">pinterest</a></li>
					</ul>
				</article>
				<!-- //Column -->
			</div>
			
			<div class="copy">
				<p>Copyright 2014, Themeenergy. All rights reserved. </p>
				
				<nav role="navigation" class="foot-nav">
					<ul>
						<li><a href="#" title="Home">Home</a></li>
						<li><a href="#" title="Blog">Blog</a></li>
						<li><a href="#" title="About us">About us</a></li>
						<li><a href="#" title="Contact us">Contact us</a></li>
						<li><a href="#" title="Terms of use">Terms of use</a></li>
						<li><a href="#" title="Help">Help</a></li>
						<li><a href="#" title="For partners">For partners</a></li>
					</ul>
				</nav>
			</div>
		</div>
	</footer>
	<!-- //Footer -->

    <!-- jQuery -->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script src="js\jquery.uniform.min.js"></script>
	<script src="js\jquery.datetimepicker.js"></script>
	<script src="js\jquery.slicknav.min.js"></script>
	<script src="js\wow.min.js"></script>
	<script src="js\search.js"></script>
	<script src="js\scripts.js"></script>
	
	<!-- TEMPLATE STYLES -->
	<div id="template-styles">
		<h2>Template Styles <a href="#"><img class="s-s-icon" src="images\settings.png" alt="Style switcher"></a></h2>
		<div>
		<h3>Colors</h3>
			<ul class="colors">
				<li><a href="#" class="beige" title="beige">beige</a></li>
				<li><a href="#" class="dblue" title="dblue">dblue</a></li>
				<li><a href="#" class="dgreen" title="dgreen">dgreen</a></li>
				<li><a href="#" class="grey" title="grey">grey</a></li>
				<li><a href="#" class="lblue" title="lblue">lblue</a></li>
				<li><a href="#" class="lgreen" title="lgreen">lgreen</a></li>
				<li><a href="#" class="lime" title="lime">lime</a></li>
				<li><a href="#" class="orange" title="orange">orange</a></li>
				<li><a href="#" class="peach" title="peach">peach</a></li>
				<li><a href="#" class="pink" title="pink">pink</a></li>
				<li><a href="#" class="purple" title="purple">purple</a></li>
				<li><a href="#" class="red" title="red">red</a></li>
				<li><a href="#" class="teal" title="teal">teal</a></li>
				<li><a href="#" class="turquoise" title="turquoise">turquoise</a></li>
				<li><a href="#" class="yellow" title="yellow">yellow</a></li>
			</ul>
		</div>
	</div>
	<script src="js\styler.js"></script>
  </body>
</html>