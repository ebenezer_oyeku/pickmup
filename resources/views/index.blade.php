﻿﻿@extends('layouts.main')

@section('content')
	<!-- Main -->
	<main class="main" role="main">
		<!-- Intro -->
		<div class="intro">
			<div class="wrap">
				<div class="textwidget">
					<h1 class="wow fadeInDown">Need a ride?</h1>
					<h2 class="wow fadeInUp">You've come to the right place.</h2>
					<div class="actions">
						<a href="#services" title="Our services" class="btn large white wow fadeInLeft anchor">Our services</a>
						<a href="#booking" title="Make a booking" class="btn large color wow fadeInRight anchor">Make a booking</a>
					</div>
				</div>
			</div>
		</div>
		<!-- //Intro -->
		
		<!-- Search -->
		<div class="advanced-search color" id="booking">
			<div class="wrap">
				<form role="form" action="search-results.html" method="post">
					<!-- Row -->
					<div class="f-row">
						<div class="form-group datepicker one-third">
							<label for="dep-date">Departure date and time</label>
							<input type="text" id="dep-date">
						</div>
						<div class="form-group select one-third">
							<label>Pick up location</label>
							<select name="state" id="state">
								@foreach($states as $state)
									<option value="{{$state->id}}">{{$state->name}}</option>
								@endforeach
							</select>
						</div>
						<div class="form-group select one-third">
							<label>Drop off location</label>
							<select  id="local">
								<option value=""></option>
							</select>
						</div>
					</div>
					<!-- //Row -->
					
					<!-- Row -->
					<div class="f-row">
						<div class="form-group datepicker one-third">
							<label for="ret-date">Return date and time</label>
							<input type="text" id="ret-date">
						</div>
						<div class="form-group select one-third">
							<label>Pick up location</label>
							<select name="state" id="state1">
								@foreach($states as $state)
									<option value="{{$state->id}}">{{$state->name}}</option>
								@endforeach
							</select>
						</div>
						<div class="form-group select one-third">
							<label>Drop off location</label>
							<select  id="local1">
								<option value=""></option>
							</select>
						</div>
					</div>
					<!-- //Row -->
					
					<!-- Row -->
					<div class="f-row">
						<div class="form-group spinner">
							<label for="people">How many people <small>(including children)</small>?</label>
							<input type="number" id="people" min="1">
						</div>
						<div class="form-group radios">
							<div>
								<input type="radio" name="radio" id="return" value="return">
								<label for="return">Return</label>
							</div>
							<div>
								<input type="radio" name="radio" id="oneway" value="oneway" checked="">
								<label for="oneway">One way</label>
							</div>
						</div>
						<div class="form-group right">
							<button type="submit" class="btn large black">Find a Pick'm'Up</button>
						</div>
					</div>
					<!--// Row -->
					</form>
				{{--</form>--}}
			</div>
		</div>
		<!-- //Search -->
		
		<!-- Services iconic -->
		<div class="services iconic white">
			<div class="wrap">
				<div class="row">
					<!-- Item -->
					<div class="one-third wow fadeIn">
						<span class="circle"><span class="ico pig"></span></span>
						<h3>Fixed rates</h3>
						<p>We provide reliable and fixed rate to all our subscribers.</p>
					</div>
					<!-- //Item -->

					<!-- Item -->
					<div class="one-third wow fadeIn" data-wow-delay=".2s">
						<span class="circle"><span class="ico lock"></span></span>
						<h3>Reliable transfers</h3>
						<p>Ability to transfer yourself, family and luggage through our pick'm'up platform at ease.</p>
					</div>
					<!-- //Item -->

					<!-- Item -->
					<div class="one-third wow fadeIn" data-wow-delay=".4s">
						<span class="circle"><span class="ico wallet"></span></span>
						<h3>No booking fees</h3>
						<p>We dont charge anyone or subscribers to book for pick'm'up request.</p>
					</div>
					<!-- //Item -->

					<!-- Item -->
					<div class="one-third wow fadeIn">
						<span class="circle"><span class="ico heart"></span></span>
						<h3>Free cancellation</h3>
						<p>Free cancellations on all pick'm'up booking with instant notification system to all parties.</p>
					</div>
					<!-- //Item -->

					<!-- Item -->
					<div class="one-third wow fadeIn" data-wow-delay=".2s">
						<span class="circle"><span class="ico wand"></span></span>
						<h3>Booking flexibility</h3>
						<p>Provide you the ability to book for our services on any mobile devices at your comfort.</p>
					</div>
					<!-- //Item -->

					<!-- Item -->
					<div class="one-third wow fadeIn" data-wow-delay=".4s">
						<span class="circle"><span class="ico telephone"></span></span>
						<h3>24h customer service</h3>
						<p>We are always ready to serve you with our 24/7 customer Service attending to your requests, complains and worries..</p>
					</div>
					<!-- //Item -->

					<!-- Item -->
					<div class="one-third wow fadeIn">
						<span class="circle"><span class="ico award"></span></span>
						<h3>Award winning service</h3>
						<p>This project is first ever in Africa and taking up it launching from Nigeria.</p>
					</div>
					<!-- //Item -->

					<!-- Item -->
					<div class="one-third wow fadeIn" data-wow-delay=".2s">
						<span class="circle"><span class="ico clip"></span></span>
						<h3>Benefits for partners</h3>
						<p>Our Partners have much benefits to enjoy in this program as long as they comply to our terms and conditions.</p>
					</div>
					<!-- //Item -->

					<!-- Item -->
					<div class="one-third wow fadeIn" data-wow-delay=".4s">
						<span class="circle"><span class="ico shuttle"></span></span>
						<h3>Quality vehicles</h3>
						<p>We guarantee you a reliable, certified vehicle for your trips with background checks on our partners..</p>
					</div>
					<!-- //Item -->
				</div>
			</div>
		</div>
		<!-- //Services iconic -->
		
		<!-- Call to action -->
		<div class="black cta">
			<div class="wrap">
				<p class="wow fadeInLeft">Do you need a reliable, secured and fixed-rate pick-up? You know what to do....</p>
				<a href="http://themeforest.net/item/transfers-transport-and-car-hire-html-template/9366018?ref=themeenergy" class="btn huge color right wow fadeInRight">Make Booking Now</a>
			</div>
		</div>
		<!-- //Call to action -->
		
		<!-- Services -->
		<div class="services boxed white" id="services">
			<!-- Item -->
			<article class="one-fourth wow fadeIn">
				<figure class="featured-image">
					<img src="images\uploads\img.jpg" alt="">
					<div class="overlay">
						<a href="{{URL::to('services')}}" class="expand">+</a>
					</div>
				</figure>
				<div class="details">
					<h4><a href="{{URL::to('services')}}">Private Pick'M'Up</a></h4>
					<p>We make your movement, journey and transportation Logistics easy by linking you up with private car partner the goes your destination or route.</p>
					<a class="more" title="Read more" href="{{URL::to('services')}}">Read more</a>
				</div>
			</article>
			<!-- //Item -->
			
			<!-- Item -->
			<article class="one-fourth wow fadeIn" data-wow-delay=".2s">
				<figure class="featured-image">
					<img src="images\uploads\bus.jpg" alt="">
					<div class="overlay">
						<a href="{{URL::to('services')}}" class="expand">+</a>
					</div>
				</figure>
				<div class="details">
					<h4><a href="{{URL::to('services')}}">Bus Pick'M'Up</a></h4>
					<p>Do you want to go for Picnic, Recreation, Beach Party, Wedding, Worship Convention, and Other Occassional Functions at a Geographical location in Group.</p>
					<a class="more" title="Read more" href="{{URL::to('services')}}">Read more</a>
				</div>
			</article>
			<!-- //Item -->
			
			<!-- Item -->
			<article class="one-fourth wow fadeIn" data-wow-delay=".4s">
				<figure class="featured-image">
					<img src="images\uploads\bus-pmu.jpg" alt="">
					<div class="overlay">
						<a href="{{URL::to('services')}}" class="expand">+</a>
					</div>
				</figure>
				<div class="details">
					<h4><a href="services.htm">Shuttle Pick'M'Up</a></h4>
					<p>You want to run errands, go to work, visit friends and family in an Affordable and Fixed price City Shuttle Bus? Get in touch with Us and we link you up with a Shuttle Bus in Minutes.</p>
					<a class="more" title="Read more" href="{{URL::to('services')}}">Read more</a>
				</div>
			</article>
			<!-- //Item -->
			
			<!-- Item -->
			<article class="one-fourth wow fadeIn" data-wow-delay=".6s">
				<figure class="featured-image">
					<img src="images\uploads\helicopter.jpg" alt="">
					<div class="overlay">
						<a href="{{URL::to('services')}}" class="expand">+</a>
					</div>
				</figure>
				<div class="details">
					<h4><a href="{{URL::to('services')}}">Helicopter/Cruise Pick'M'Up</a></h4>
					<p>Do you need an Adventure on Air or needs to meet up a business meeting but you are very behind schedule? Get in touch with us and we pick up in munites away.</p>
					<a class="more" title="Read more" href="services.htm">Read more</a>
				</div>
			</article>
			<!-- //Item -->			
		</div>
		<!-- //Services -->
		
		<!-- Testimonials -->
		<div class="testimonials center black">
			<div class="wrap">
				<h6 class="wow fadeInDown">Wow, Innovative Approach to Reliable Transport!</h6>
				<p class="wow fadeInUp">A developed country is not a place where the poor have cars. It’s where the rich use public transport.</p>
				<p class="meta wow fadeInUp">-Enrique Penalosa, former Mayor of Bogotá, Colombia</p>
			</div>
		</div>
		<!-- //Testimonials -->
		
		<div class="partners white center">
			<div class="wrap">
				<h2 class="wow fadeIn">Our partners</h2>
				<div class="one-fifth wow fadeIn"><a href="#"><img src="images\uploads\logo1.jpg" alt=""></a></div>
				<div class="one-fifth wow fadeIn" data-wow-delay=".2s"><a href="#"><img src="images\uploads\logo2.jpg" alt=""></a></div>
				<div class="one-fifth wow fadeIn" data-wow-delay=".4s"><a href="#"><img src="images\uploads\logo3.jpg" alt=""></a></div>
				<div class="one-fifth wow fadeIn" data-wow-delay=".6s"><a href="#"><img src="images\uploads\logo4.jpg" alt=""></a></div>
				<div class="one-fifth" data-wow-delay=".8s"><a href="#"><img src="images\uploads\logo5.jpg" alt=""></a></div>
			</div>
		</div>
		
		
		<!-- Call to action -->
		<div class="color cta">
			<div class="wrap">
				<p class="wow fadeInLeft">Like what you see? Are you ready to stand out? You know what to do.</p>
				<a href="http://themeforest.net/item/transfers-transport-and-car-hire-html-template/9366018?ref=themeenergy" class="btn huge black right wow fadeInRight">Purchase theme</a>
			</div>
		</div>
		<!-- //Call to action -->
	</main>
	<!-- //Main -->
@endsection

@section('script')
	<script>

		$('#state').on('change',function(e){
			console.log(e);

			var state_id = e.target.value;

			//ajax
			$.get('/ajax-loc?state_id=' + state_id, function(data){
				//success data
				$('#local').empty();
				$.each(data, function(index, locObj){

					$('#local').append('<option value="'+locObj.id+'">'+locObj.name+'</option>');
				});
			});
		});

		$('#state1').on('change',function(e){
			console.log(e);

			var state_id = e.target.value;

			//ajax
			$.get('/ajax-loc?state_id=' + state_id, function(data){
				//success data
				$('#local1').empty();
				$.each(data, function(index, locObj){

					$('#local1').append('<option value="'+locObj.id+'">'+locObj.name+'</option>');
				});
			});
		});
	</script>
	@endsection