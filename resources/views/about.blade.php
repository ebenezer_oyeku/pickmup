﻿@extends('layouts.page')

@section('title')
	About Our Transport-Hub
@endsection

@section('content')
	<!-- Main -->
	<main class="main" role="main">
		<!-- Page info -->
		<header class="site-title color">
			<div class="wrap">
				<div class="container">
					<h1>About us</h1>
					<nav role="navigation" class="breadcrumbs">
						<ul>
							<li><a href="index-1.htm" title="Home">Home</a></li>
							<li>About us</li>
						</ul>
					</nav>
				</div>
			</div>
		</header>
		<!-- //Page info -->
		
		<div class="wrap">
			<div class="row">
				<!--- Content -->
				<div class="content three-fourth textongrey">
					<h2>Our story</h2>
					<p class="lead">From  tha Ancient city of Ibadan, the most largest City in West Africa. Looking close at the Moon during a cool night breeze, i was think on the advancement Technology has brought on our transportation system around the world. But it seems not everyone is enjoying the innovative idea the inventor pput in place to make life comfortable to all.</p>
					<p>The idea came up to me that their are needs for us innovate a system that will bridge the gaps in unrealistic transport system that has always existed in Africa, most especially Nigeria. After a long brainstorming and research, we came up with the <strong>Pick'm'Up</strong> platform that is design to solve transportation inconsistency in rates and disappointments</p>
					<p>The project is designed to help everyone to have access to cheap and affordable rate of transport system in the country. Africa is a focus of growth, in other to make Africa Our pride we all need to get involve in creating a conducive and organized transport system for all. Our plan on this project is to prototype and test this system in Nigeria for few months to check against success which will attract investor all around the world and will help us spread the project to many African Countries</p>
					<h3>Our mission</h3>
					<p>Building a reliable transportation services and System with engagement of everyone as a partner and entrepreneur. </p>
					<h3>Our vision</h3>
					<p>Creating Unified Transport System for All human that showcase the act of love and sharing.</p>
				</div>
				<!--- //Content -->
				
				<!--- Sidebar -->
				<aside class="one-fourth sidebar right offset">
					<!-- Widget -->
					<div class="widget">
						<h4>Why book with us?</h4>
						<div class="textwidget">
							<h5>Reliable and Safe</h5>
							<p>Adding reliability and Security to transport systems to increase life expectancy.</p>
							<h5>No hidden fees</h5>
							<p>Focusing on the needy by making life easier through affordable transport system.</p>
							<h5>We’re Always Here</h5>
							<p>Everyone is very important to us, so the project is designed to serve you all.</p>
						</div>
					</div>
					<!-- //Widget -->
					
					<!-- Widget -->
					<div class="widget">
						<h4>Advertisment</h4>
						<a href="#"><img src="images\uploads\advertisment.jpg" alt=""></a>
					</div>
					<!-- //Widget -->
				</aside>
				<!--- //Sidebar -->
			</div>
		</div>
	</main>
	<!-- //Main -->
@endsection

@section('s-icon')
	<!-- Services iconic -->
	<div class="services iconic white">
		<div class="wrap">
			<div class="row">
				<!-- Item -->
				<div class="one-third wow fadeIn">
					<span class="circle"><span class="ico pig"></span></span>
					<h3>Fixed rates</h3>
					<p>We provide reliable and fixed rate to all our subscribers.</p>
				</div>
				<!-- //Item -->

				<!-- Item -->
				<div class="one-third wow fadeIn" data-wow-delay=".2s">
					<span class="circle"><span class="ico lock"></span></span>
					<h3>Reliable transfers</h3>
					<p>Ability to transfer yourself, family and luggage through our pick'm'up platform at ease.</p>
				</div>
				<!-- //Item -->

				<!-- Item -->
				<div class="one-third wow fadeIn" data-wow-delay=".4s">
					<span class="circle"><span class="ico wallet"></span></span>
					<h3>No booking fees</h3>
					<p>We dont charge anyone or subscribers to book for pick'm'up request.</p>
				</div>
				<!-- //Item -->

				<!-- Item -->
				<div class="one-third wow fadeIn">
					<span class="circle"><span class="ico heart"></span></span>
					<h3>Free cancellation</h3>
					<p>Free cancellations on all pick'm'up booking with instant notification system to all parties.</p>
				</div>
				<!-- //Item -->

				<!-- Item -->
				<div class="one-third wow fadeIn" data-wow-delay=".2s">
					<span class="circle"><span class="ico wand"></span></span>
					<h3>Booking flexibility</h3>
					<p>Provide you the ability to book for our services on any mobile devices at your comfort.</p>
				</div>
				<!-- //Item -->

				<!-- Item -->
				<div class="one-third wow fadeIn" data-wow-delay=".4s">
					<span class="circle"><span class="ico telephone"></span></span>
					<h3>24h customer service</h3>
					<p>We are always ready to serve you with our 24/7 customer Service attending to your requests, complains and worries..</p>
				</div>
				<!-- //Item -->

				<!-- Item -->
				<div class="one-third wow fadeIn">
					<span class="circle"><span class="ico award"></span></span>
					<h3>Award winning service</h3>
					<p>This project is first ever in Africa and taking up it launching from Nigeria.</p>
				</div>
				<!-- //Item -->

				<!-- Item -->
				<div class="one-third wow fadeIn" data-wow-delay=".2s">
					<span class="circle"><span class="ico clip"></span></span>
					<h3>Benefits for partners</h3>
					<p>Our Partners have much benefits to enjoy in this program as long as they comply to our terms and conditions.</p>
				</div>
				<!-- //Item -->

				<!-- Item -->
				<div class="one-third wow fadeIn" data-wow-delay=".4s">
					<span class="circle"><span class="ico shuttle"></span></span>
					<h3>Quality vehicles</h3>
					<p>We guarantee you a reliable, certified vehicle for your trips with background checks on our partners..</p>
				</div>
				<!-- //Item -->
			</div>
		</div>
	</div>
	<!-- //Services iconic -->
	
	<!-- Call to action -->
	<div class="color cta">
		<div class="wrap">
			<p class="wow fadeInLeft">Like what you see? Are you ready to stand out? You know what to do.</p>
			<a href="#" class="btn huge black right wow fadeInRight">Purchase theme</a>
		</div>
	</div>
	<!-- //Call to action -->
@endsection

@section('script')
    <!-- jQuery -->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
@endsection