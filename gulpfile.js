var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

var bower_path = "resources/assets/vendor";

var path = {
    "bootstrap" : bower_path + "/bootstrap",
    "jquery" : bower_path + "/jquery",
    "datatables" : bower_path + "/datatables",
    "pickmup" : bower_path + "/pickmup"
}

elixir(function(mix) {

//Mix Styles
    mix.styles([

        path.bootstrap + '/dist/css/bootstrap.css',
        path.datatables + '/media/css/jquery.dataTables.css',
        path.pickmup + '/css/pickmup.css'

    ], 'public/css/pickmup.css', './')

    //Mix Scripts
    mix.scripts([

        path.jquery + '/dist/jquery.js',
        path.bootstrap + '/dist/js/bootstrap.js',
        path.datatables + '/media/js/jquery.dataTables.js',
        path.pickmup + '/js/pickmup.js'

    ], 'public/js/pickmup.js', './')


    //Copy Fonts
    mix.copy([
        path.bootstrap + '/fonts',
    ], 'public/fonts');

    //Copy Images
    mix.copy([
        path.datatables + 'images',
        path.pickmup + '/images',
    ], 'public/images');
    //mix.sass('app.scss');
});
