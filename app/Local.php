<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Local extends Model
{
    protected $table = 'locals';
    protected $fillable = ['local_name', 'state_id'];

    public function state()
    {
        return $this->belongsTo('State');
    }
}
