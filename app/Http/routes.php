<?php

use App\Local;
use Illuminate\Support\Facades\Input;

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('users/{user}', function (App\User $user) {
    return $user;
});

Route::get('/', 'IndexController@index');
//Route::get('/', function () {
//    return view('index');
//});

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    //
});



Route::resource('index', 'IndexController');
Route::resource('about', 'AboutController');
Route::resource('services', 'ServiceController');
Route::resource('contact', 'ContactController');
Route::auth();

Route::get('/home', 'HomeController@index');


Route::get('/ajax-loc',function(){
    $state_id = Input::get('state_id');
    $locals = Local::where('state_id', '=', $state_id)->get();
    return Response::json($locals);
});
