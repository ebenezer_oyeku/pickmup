<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use GeoIP as GeoIP;

class GeoIPController extends Controller
{
    public function start() {
        $places = [];

        for($i = 0; $i < 20; $i++ ) {

            $octet1 = rand(1,255);
            $octet2 = rand(1,255);
            $octet3 = rand(1,255);
            $octet4 = rand(1,255);

            $location = GeoIP::getLocation($octet1.'.'.$octet2.'.'.$octet3.'.'.$octet4);
            $places[] = $location;
        }


//        dd($places);
        return view('welcome')->with(['places' => $places]);
    }
}
